# spark-test

#### 项目介绍
spark java和scala混编

#### 软件架构
spark scala的测试程序
添加ambari环境下 直接用IDEA提交spark程序的方式


#### 安装教程

IDEA直接提交ambari saprk的方法
1. 将ambari平台的配置文件 core-site.xml,hadoop-env.sh,hdfs-site.xml,mapred-env.sh,mapred-site.xml,yarn-env.sh,yarn-site.xml
  拷贝到resources目录下
  
2. spark的master 设置 sparkConf.setMaster("yarn-client")

3. 设置hdp的版本号 3.1.0.0-78 和hdp保持一致
    .set("spark.driver.extraJavaOptions","-Dhdp.version=3.1.0.0-78")
    .set("spark.yarn.am.extraJavaOptions","-Dhdp.version=3.1.0.0-78")
    
4. pom的依赖中需要增加spark-yarn_2.11
    <dependency>
        <groupId>org.apache.spark</groupId>
        <artifactId>spark-yarn_2.11</artifactId>
        <version>${spark.version}</version>
    </dependency>
    
5. 需要修改mapreduce-site文件中的${hdp.version}为实际的版本号

6. 修改mapreduce-site.xml文件里的topology_script.py项，直接去掉

7.  将spark需要jar上传到hdfs中
    hdfs dfs -mkdir -p /user/spark/share/lib
    hdfs dfs -put ./*.jar /user/spark/share/lib/
    
    在代码中添加读取jars的路径配置 
    .set("spark.yarn.jars", "hdfs://db03:8020/user/spark/share/lib/*.jar")


#### 出现问题

1.  抄不到jersey文件包
    在pom的依赖中需要增加jersey-client依赖
    <dependency>
        <groupId>com.sun.jersey</groupId>
        <artifactId>jersey-client</artifactId>
        <version>1.9</version>
    </dependency>
    
2.  Cannot run program "/etc/hadoop/conf/topology_script.py" (in directory "D:\workspace\fawmc-new44\operation-report-calc"): CreateProcess error=2, 系统找不到指定的文件。
    在core-site.xml中注释掉如下:
    <property>
      <name>net.topology.script.file.name</name>
      <value>/etc/hadoop/conf/topology_script.py</value>
    </property>    

3.  Could not find or load main class org.apache.spark.deploy.yarn.ExecutorLauncher
    pom添加如下:
    <dependency>
        <groupId>org.apache.spark</groupId>
        <artifactId>spark-assembly_2.10</artifactId>
        <version>1.1.1</version>
    </dependency>

4.  Could not initialize class org.apache.derby.jdbc.EmbeddedDriver
    pom添加如下:
    <dependency>
        <groupId>org.apache.derby</groupId>
        <artifactId>derby</artifactId>
        <version>10.9.1.0</version>
    </dependency>

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)