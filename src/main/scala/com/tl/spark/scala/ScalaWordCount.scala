package com.tl.spark.scala


import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @program: spark-test
  * @description:
  * @author: dong.tl
  * @create: 2018-09-12 13:39
  **/
object ScalaWordCount {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("ScalaWordCount").setMaster("local[2]")


    val sc = new SparkContext(conf)

    val strings = Array("1", "2")

//    sc.parallelize(strings).flatMap(s => {
//      if (s.equals("1"))
//        Array()
//      //        return
//      else
//        s.split("")
//    }).foreach(println(_))

    //    val value: RDD[String] = sc.textFile(args(0)).flatMap(_.split(" "))
    //    val strings: Array[Int] = Array(1, 2, 3)
    //    var sres = sc.textFile(args(0)).flatMap(s=>{})
    //
    //
        var res = sc.textFile(args(0)).flatMap(_.split(" ")).map((_, 1)).reduceByKey(_ + _).sortBy(_._2, false).saveAsTextFile(args(1))
    //    res.foreach(println)

    sc.stop()


  }
}
