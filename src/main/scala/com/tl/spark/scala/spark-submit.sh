#!/bin/bash

base_path=/data/service/spark-workspace
version=1.0-SNAPSHOT
jar_name=spark-etl_2.11-${version}-assembly.jar
class_name=com.spark.etl.Application
# client or cluste
rdeploy_mode=cluster
cmd="spark-submit --class ${class_name} --master yarn --deploy-mode ${deploy_mode} --driver-cores 1 --driver-memory 2G --num-executors 4 --executor-cores 2 --executor-memory 4G --driver-java-options \"-Dlog4j.configuration=log4j-driver.properties\" --conf spark.executor.extraJavaOptions=\"-Dlog4j.configuration=log4j-executor.properties\" --files ${base_path}/log4j-executor.properties,${base_path}/log4j-driver.properties ${base_path}/${jar_name}"
echo "${cmd}"

